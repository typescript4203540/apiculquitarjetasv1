 // Definición de la clase
export class ValidacionCorreo {
    // Método público
    public esCorreoValido(correo: string): boolean {
        // Expresión regular para validar el correo con los dominios permitidos
        const patron = /^[a-zA-Z0-9._%+-]+@(gmail\.com|hotmail\.com|yahoo\.es)$/;
      
        // Usamos el método test() para verificar si el correo coincide con el patrón
        return patron.test(correo);
      }
  }
  
  // Función exportada que crea una instancia de la clase y la exporta
  export function createMyClassInstance(): ValidacionCorreo {
    return new ValidacionCorreo();
  }
  