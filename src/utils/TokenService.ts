// Definición de la clase
export class TokenService {
    // Método público
    public generateToken(): string {
        // Generar una cadena aleatoria de 16 caracteres (por ejemplo, utilizando letras y números)
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let token = '';
        for (let i = 0; i < 16; i++) {
          const randomIndex = Math.floor(Math.random() * characters.length);
          token += characters.charAt(randomIndex);
        }
        console.log("token: "+token); 
        return token;
      }
  }
  
  // Función exportada que crea una instancia de la clase y la exporta
  export function createMyClassInstance(): TokenService {
    return new TokenService();
  }
  