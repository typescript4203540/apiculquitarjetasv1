export class Constantes {
    static readonly IP_SERVER_REDIS: string = "127.0.0.1";
    static readonly PUERTO_REDIS: number = 6379;
    static readonly VIGENCIA_TOKEN_MINUTOS: number = 15; 
    static readonly VIGENCIA_TOKEN_MILISEGUNDOS: number = Constantes.VIGENCIA_TOKEN_MINUTOS * 60000;
    static readonly SECRET_PK: string = 'pk_test_LsBRKejzCOEEWOsw';
    static readonly ES_NUMERO_TARJETA_INVALIDA: string = "-1";
    static readonly EXPIRACION_MAXIMA: number = 5; 
    static readonly ES_TARJETA_CON_EXPIRACION_INVALIDA: string = "-2";
    static readonly ES_CORREO_INVALIDO: string = "-3";
  }
