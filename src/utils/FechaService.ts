// Definición de la clase
export class FechaService {
    // Método público
    public agregarMinutos(fecha: Date, minutos: number): Date {
        const nuevaFecha = new Date(fecha);
        nuevaFecha.setMinutes(nuevaFecha.getMinutes() + minutos);
        return nuevaFecha;
      }
  }
  
  // Función exportada que crea una instancia de la clase y la exporta
  export function createMyClassInstance(): FechaService {
    return new FechaService();
  }
  