// Definición de la clase
export class ValidacionTarjeta {
    // Método público
    public validarLuhn(numero: number): boolean {
        // Convierte el número en una cadena y revierte sus dígitos
        const numeroCadena = numero.toString().split('').reverse().join('');

        let suma = 0;
        let doblar = false;

        for (let i = 0; i < numeroCadena.length; i++) {
            let digito = parseInt(numeroCadena[i], 10);

            if (doblar) {
                digito *= 2;

                if (digito > 9) {
                    digito -= 9;
                }
            }

            suma += digito;
            doblar = !doblar;
        }

        return suma % 10 === 0;
    }
}
// Función exportada que crea una instancia de la clase y la exporta
export function createMyClassInstance(): ValidacionTarjeta {
    return new ValidacionTarjeta();
}
