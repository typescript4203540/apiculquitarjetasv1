import * as http from 'http';
import * as url from 'url';
import { AuthenticateService } from './services/AuthenticateService';
import CardRequest from './models/CardRequest';
import { Constantes } from './utils/Constantes';
import { obtenerCardDesdeRedis } from './db/RedisOperation';

// Crear un servidor HTTP.
const server = http.createServer(async (req, res) => {
  // Configuración de CORS
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader('Access-Control-Allow-Headers', '*');
  const { pathname } = url.parse(req.url || "", true);
  const pk = req.headers["pk"];
  if (pathname === "/tokens" && req.method === "POST") {
    let body = "";
    req.on("data", (chunk) => {
      body += chunk.toString();
    });
    req.on("end", () => {
      try {
        const { email, card_number, cvv, expiration_year, expiration_month } =
          JSON.parse(body);
        const authenticateService = new AuthenticateService();
        const cardRequest = new CardRequest(
          email,
          card_number,
          cvv,
          expiration_year,
          expiration_month
        );
        const token = authenticateService.authenticate(cardRequest, pk);
        if (
          token != Constantes.ES_NUMERO_TARJETA_INVALIDA &&
          token != Constantes.ES_TARJETA_CON_EXPIRACION_INVALIDA &&
          token != Constantes.ES_CORREO_INVALIDO
        ) {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(JSON.stringify({ token }));
        } else {
          if (token === Constantes.ES_NUMERO_TARJETA_INVALIDA) {
            res.writeHead(400, { "Content-Type": "text/plain" });
            res.end("Número de tarjeta inválida");
          } else {
            if (token === Constantes.ES_TARJETA_CON_EXPIRACION_INVALIDA) {
              res.writeHead(400, { "Content-Type": "text/plain" });
              res.end(
                "Fecha de expiración de tarjeta excede el maximo permitido"
              );
            } else {
              if (token === Constantes.ES_CORREO_INVALIDO) {
                res.writeHead(400, { "Content-Type": "text/plain" });
                res.end("Correo inválido");
              } else {
                res.writeHead(401, { "Content-Type": "text/plain" });
                res.end("Credenciales inválidas");
              }
            }
          }
        }
      } catch (error) {
        res.writeHead(400, { "Content-Type": "text/plain" });
        res.end("Solicitud inválida");
      }
    });
  } else if (pathname === "/cards" && req.method === "GET") {
    const pk = req.headers["pk"];
    let body = "";
    req.on("data", (chunk) => {
      body += chunk.toString();
    });
    req.on("end", async () => {
      console.log("body");
      console.log(body);
      console.log("--------------------------");
      const { token } = JSON.parse(body);
      if (!token) {
        res.writeHead(401, { "Content-Type": "text/plain" });
        res.end("Token de autorización faltante");
      } else {
        if (pk === Constantes.SECRET_PK) {
          const searched_card = await obtenerCardDesdeRedis(token);
          if (searched_card === null) {
            console.log(
              `No se encontró ningún objeto con la clave ${token} en Redis.`
            );
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end("Token expirado");
          } else {
            console.log("Objeto obtenido desde Redis:", searched_card);
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify(searched_card));
          }
        } else {
          res.writeHead(401, { "Content-Type": "text/plain" });
          res.end("Apk de autorización inválido");
        }
      }
    });
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("Ruta no encontrada");
  }
});

// Define el puerto en el que escuchará el servidor
const port = process.env.PORT || 3000;
// Iniciar el servidor en el puerto 3000.
server.listen(port, () => {
  console.log('Servidor en ejecución en el puerto 3000');
  //imprimirObjetosDesdeRedis();
});