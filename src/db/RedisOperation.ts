import Card from '../models/Card';
import { RedisConnection } from './RedisConnection';

// Función para guardar un objeto en Redis
export async function guardarObjetoEnRedis(key: string, objeto: Card): Promise<void> {
  // Crea una instancia de cliente Redis
  const redis = RedisConnection();
  try {
    // Convierte el objeto a una cadena JSON    
    const objetoSerializado = JSON.stringify(objeto);
    // Usa el comando SET para guardar el objeto en Redis
    await redis.then((conexion_redis) => 
    {
      conexion_redis.set(key, objetoSerializado);
    });      
    console.log(`Objeto guardado en Redis con clave: ${key}`);
  } catch (error) {
    console.error('Error al guardar el objeto en Redis:', error);
    throw error;
  } finally {
    // Cierra la conexión Redis cuando hayas terminado
    await redis.then((conexion_redis) => 
    {
      conexion_redis.quit();
    });
  }
}

// Función para imprimir un objeto desde Redis dado su clave
export async function imprimirObjetoDesdeRedis(clave: string): Promise<void> {
  // Crea una instancia de cliente Redis
  const redisConnection = RedisConnection();
  await redisConnection.then(async (redis_connection) => 
  {
    const redis = redis_connection;
    try {
      // Usa el comando GET para obtener el objeto desde Redis
      const objetoSerializado = await redis.get(clave);
  
      if (objetoSerializado) {
        // Convierte la cadena JSON de vuelta a un objeto
        const objeto = JSON.parse(objetoSerializado);
        console.log('Objeto obtenido desde Redis:', objeto);
      } else {
        console.log('La clave no existe en Redis:', clave);
      }
    } catch (error) {
      console.error('Error al obtener el objeto desde Redis:', error);
      throw error;
    } finally {
      // Cierra la conexión Redis cuando hayas terminado
      redis.quit();
    }
  });
}

export async function imprimirObjetosDesdeRedis() {
  const redis = await RedisConnection();
  try {
    console.log(`----------------------------------------`);
    console.log(`----------------------------------------`);
    console.log(`IMPRIME TODAS LAS TAREJETAS ALMACENADAS`);
    console.log(`----------------------------------------`);
    console.log(`----------------------------------------`);
    // Obtiene todas las claves en la base de datos Redis
    const claves = await redis.keys('*');

    // Itera a través de las claves y obtiene los valores correspondientes
    for (const clave of claves) {
      const valor = await redis.get(clave);
      console.log(`Clave: ${clave}, Valor: ${valor}`);
    }
    console.log(`----------------------------------------`);
    console.log(`----------------------------------------`);
  } catch (error) {
    console.error('Error al imprimir claves y valores:', error);
  } finally {
    // Cierra la conexión Redis cuando hayas terminado
    redis.quit();
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function quitarAtributo(objeto: any, atributo: string): any {
  const copiaObjeto = { ...objeto }; // Copia el objeto original para no modificarlo directamente
  delete copiaObjeto[atributo]; // Elimina el atributo del objeto copiado
  return copiaObjeto; // Devuelve el objeto sin el atributo
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function obtenerAtributo(objeto: any, atributo: string): any {
  return objeto[atributo];
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function tokenExpirado(card: Card): boolean {
  const fecha_expiracion = new Date(obtenerAtributo(card, 'fecha_expiracion'));
   const fechaActual = new Date();
  // Resta las dos fechas y obtiene la diferencia en milisegundos
  const diferenciaEnMilisegundos: number = (fechaActual.getTime() - fecha_expiracion.getTime());

  //console.log('Diferencia en milisegundos:', diferenciaEnMilisegundos);
  return diferenciaEnMilisegundos >= 0 ? true : false ; // Devuelve el objeto sin el atributo
}

// Función para imprimir un objeto desde Redis dado su clave
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function obtenerCardDesdeRedis(clave: string): Promise<any | null> {
  const redis = await RedisConnection();
  try {
    // Utiliza el comando GET para obtener el valor de Redis
    const valor = await redis.get(clave);

    if (valor === null  || tokenExpirado(JSON.parse(valor))) {
      // La clave no existe en Redis
      return null;
    }

    // Parsea el valor JSON
    const card = JSON.parse(valor);
    const cardSinCVV = quitarAtributo(card, 'cvv');
    const cardSinID = quitarAtributo(cardSinCVV, 'unique_id');
    const cardSinTOKEN= quitarAtributo(cardSinID, 'token');
    const cardSinFC= quitarAtributo(cardSinTOKEN, 'fecha_creacion');
    const cardSinFE= quitarAtributo(cardSinFC, 'fecha_expiracion');
    const cardFinal = cardSinFE;
    return cardFinal;
  } catch (error) {
    console.error('Error al consultar valor en Redis:', error);
    throw error;
  }
}