import Redis from 'ioredis';
import { Constantes } from '../utils/Constantes';

export async function RedisConnection(): Promise<Redis> {
  try {
    //console.log('Inicio de Conexión a Redis');

    const redisClient = new Redis({
      host: Constantes.IP_SERVER_REDIS, // Cambia esto si tu servidor Redis no está en localhost
      port: Constantes.PUERTO_REDIS, // Puerto predeterminado de Redis
      // Otras opciones de configuración, si es necesario
    });

    return redisClient;
      } catch (error) {
        console.error('Error al conectar a Redis en Redis.ts:', error);
        throw error;
  }
  }
