import { TokenService } from '../utils/TokenService';
import { FechaService } from '../utils/FechaService';
import CardRequest from '../models/CardRequest';
import { guardarObjetoEnRedis } from '../db/RedisOperation';
import Card from '../models/Card';
import { Constantes } from '../utils/Constantes';
import { ValidacionTarjeta } from '../utils/ValidacionTarjeta';
import { ValidacionCorreo } from '../utils/ValidacionCorreo';

// Definición de la clase
export class AuthenticateService {
  // Método público
  // Función para verificar la autenticación y generar un token JWT si es exitosa.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public authenticate(cardRequest: CardRequest, pkComercio: any): string | null {
    const validacionTarjeta = new ValidacionTarjeta();
    if (!validacionTarjeta.validarLuhn(cardRequest.card_number)) {
      return Constantes.ES_NUMERO_TARJETA_INVALIDA;
    }
    const fecha = new Date(); // Obtiene la fecha y hora actual
    if (parseInt(cardRequest.expiration_year)-fecha.getFullYear()>Constantes.EXPIRACION_MAXIMA) {
      return Constantes.ES_TARJETA_CON_EXPIRACION_INVALIDA;
    }
    
    const validacionCorreo = new ValidacionCorreo();
    if (!validacionCorreo.esCorreoValido(cardRequest.email)) {
      return Constantes.ES_CORREO_INVALIDO;
    }

    const fechaOriginal = new Date();
    const fechaService = new FechaService();
    const nuevaFecha = fechaService.agregarMinutos(fechaOriginal, Constantes.VIGENCIA_TOKEN_MINUTOS);
    const tokenService = new TokenService();
    const token = tokenService.generateToken();
    const card = new Card(token,
      cardRequest.email,
      cardRequest.card_number,
      cardRequest.cvv,
      cardRequest.expiration_year,
      cardRequest.expiration_month,
      token,
      fechaOriginal,
      nuevaFecha,
      pkComercio);
    guardarObjetoEnRedis(token, card);
    return token;
  }
}

// Función exportada que crea una instancia de la clase y la exporta
export function createMyClassInstance(): AuthenticateService {
  return new AuthenticateService();
}
