class CardRequest {
    public email: string;
    public card_number: number;
    public cvv: number;
    public expiration_year: string;
    public expiration_month: string;
    constructor(
      email: string,
      card_number: number,
      cvv: number,
      expiration_year: string,
      expiration_month: string,
    ) {
      this.email = email;
      this.card_number = card_number;
      this.cvv = cvv;
      this.expiration_year = expiration_year;
      this.expiration_month = expiration_month;
    }
  }
  
  export default CardRequest;
  