class Card {
    public unique_id: string;
    public email: string;
    public card_number: number;
    public cvv: number;
    public expiration_year: string;
    public expiration_month: string;
    public token: string;
    public fecha_creacion: Date;
    public fecha_expiracion: Date;
    public pk_comercio: string;  
    constructor(
      unique_id: string,
      email: string,
      card_number: number,
      cvv: number,
      expiration_year: string,
      expiration_month: string,
      token: string,
      fecha_creacion: Date,
      fecha_expiracion: Date,
      pk_comercio: string
    ) {
      this.unique_id = unique_id;
      this.email = email;
      this.card_number = card_number;
      this.cvv = cvv;
      this.expiration_year = expiration_year;
      this.expiration_month = expiration_month;
      this.token = token;
      this.fecha_creacion = fecha_creacion;
      this.fecha_expiracion = fecha_expiracion;
      this.pk_comercio = pk_comercio;
    }
  }
  
  export default Card;
  