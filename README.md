Proyecto:

"build": "tsc", para compilar el proyecto

"test": "jest", para ejecutar pruebas jest

"start": "ts-node src/app.ts", para iniciar el proyecto

"linter": "npx eslint src" para correr pruebas linter

Redis:

For windows 64 go here https://github.com/MicrosoftArchive/redis/releases and

=>Download Redis-x64-3.2.100.msi file

=>Run it and install it

=>Then open the Redis folder (it will the installed folder) then run "redis-server.exe" file.

=>Now your Redis server is activated

=>You can check this by run "redis-cli.exe" file and type ping it will be pong.
